# CLEAN #

`Clean` est un thème de blog pour Pelican, adapté du template du même nom trouvable sur [FREEHTML5.co](http://freehtml5.co/).

# Configuration #

  * `SLOGAN`: texte affiché dans un encart sur la homepage

# Plugins #

Il est prêt pour accueillir `lightbox`.


# Licence #

MIT.
